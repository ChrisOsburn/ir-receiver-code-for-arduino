/* Author: Chris Osburn
 * Date: 4/16/20
 * Description: This simple code is intended for detection of an IR LED by a photodiode. 
 * Upon detection an LED will light up, else it will be off
 */

void setup() {
  //start serial connection
  Serial.begin(9600);
  //configure pin 2 as an input and enable the internal pull-up resistor
  pinMode(2, INPUT_PULLUP);
  //configure pin 3 as low output for LED
  pinMode(3, OUTPUT);
  digitalWrite(3, LOW);

}

void loop() {
  //read the reciever circuit input
  int sensorVal = digitalRead(2);

  //if the sensor is read, turn on LED. Else turn off LED
  if (sensorVal == 0) {
    Serial.print("high\n");
    digitalWrite(3, HIGH);
  } else {
    Serial.print("low\n");
    digitalWrite(3, LOW);
  }
}